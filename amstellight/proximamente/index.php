<?php
//	header("charset=utf-8");
	$id_page=2;
	$clase='contador';
	include_once '../functions.php';
	include_once '../header.php';
?>

<div id="wraper">
<!--Yo puse esto-->
	<div id="logo"></div>
	<div class="contador" role="main">
		<div class="clock" title="Próximamente, Amstel Light"></div>
		<div class="message"></div>
	</div>
	<div id="social">
		<a href="#" target="_blank" title="Amstel Light Facebook"><img src="<?=$rutaimg;?>fb.png" alt="Amstel Light Facebook" /></a>
		<a href="#" target="_blank" title="Amstel Light Twitter"><img src="<?=$rutaimg;?>tw.png" alt="Amstel Light Twitter" /></a>
		<a href="#" target="_blank" title="Amstel Light Youtube"><img src="<?=$rutaimg;?>yt.png" alt="Amstel Light Youtube" /></a>
	</div>
	<footer class="proximamente">
		<span class="nc" role="navigation">
			<a href="<?=$ruta?>aviso-de-privacidad/" target="_blank" title="Abrir el Aviso de privacidad">Aviso de privacidad</a>
			<a href="<?=$ruta?>terminos-y-condiciones/" target="_blank" title="Abrir los Términos y condiciones">Términos y condiciones</a>
			<a href="http://cuamoc.com/" target="_blank" title="Abrir Cuauhtémoc Moctezuma">&copy;2015 Cuauhtémoc Moctezuma</a>
			<a class="none" title="Cofepris">153300201A1933</a>
			<a class="none" title="Evita el exceso">Evita el exceso</a>
		</span>
		<span class="nx">
			<a href="http://www.heineken.com/mx/home.aspx" target="_blank" title="Heineken"><img src="<?=$rutaimg;?>hkm.png" alt="Heineken" /></a>
			<a href="https://www.facebook.com/ConsumoInteligenteMX" target="_blank" title="Consumo inteligente"><img src="<?=$rutaimg;?>cmoderado.png" alt="Consumo inteligente" /></a>
		</span>
	</footer>
</div>
<script charset="utf-8">
	setInterval(function(){
		fondo++;
		if(fondo==1)
			$("body").css("background-image", "url('../img/b1.jpg')");
		else if(fondo==2)
			$("body").css("background-image", "url('../img/b2.jpg')");
		else if(fondo==3){
			$("body").css("background-image", "url('../img/b3.jpg')");
			fondo=0;
		}
	},8000);
</script>

<?php include_once '../footer.php'; ?>