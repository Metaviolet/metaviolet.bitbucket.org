<?php
	$id_page=4;
	$clase='terminos';
	include_once '../functions.php';
	include_once '../header.php';
?>

<div id="wraper">
	<div class="terminos">
		<img src="<?=$rutaimg;?>logo.png" />
		<h1>Aviso de privacidad</h1>
		<p>En atención a las disposiciones contenidas en la Ley Federal de Protección de Datos Personales en
		Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad ("Ley")
		vigentes en México, le informamos que este Aviso de Privacidad de Datos Personales ("Aviso")
		describe como Cervecería Cuauhtémoc Moctezuma, S.A. de C.V. o cualquiera de sus empresas filiales
		o subsidiarias en los Estados Unidos Mexicanos (“CCM”) recolectará, tratará, almacenará y, en general,
		usará la información proporcionada por Usted (“Usuario(s)”) a través de este formato y medios
		oficiales de CCM (“Medios”). Para efectos de este Aviso, el domicilio de CCM se constituye en
		Avenida Alfonso Reyes No. 2202 Norte en la Colonia Bellavista en la ciudad de Monterrey, N.L.,
		C.P. 64410.</p>
		<p>El término "Información Personal" usado en este Aviso significa la información que identifica
		al Usuario personalmente, incluyendo datos de identificación tales como nombre, apellido, domicilio,
		números telefónicos, correo electrónico, género y edad, manifestando que no se le solicitarán datos
		catalogados como sensibles según la Ley.</p>
		<ol>
			<li>CCM recopila la Información Personal del Usuario con los siguientes propósitos: (a) fines de
			identificación, (b) fines estadísticos, (c) fines promocionales y comerciales, (d) suministrar
			información personal del Usuario a las entidades con las que CCM tiene contratado algún convenio
			de prestación de servicios, (e) prestar los servicios que se ofrezcan a través de los Medios que
			CCM estime convenientes y (f) suministrar la información personal del Usuario a las autoridades
			competentes, ya sea por orden o acuerdo de CCM con dichas autoridades (“Tratamiento”). En la
			recolección de datos personales se siguen los principios en la forma que establece la Ley: licitud,
			calidad, consentimiento, información, finalidad, lealtad, proporcionalidad y responsabilidad.
			CCM sólo obtendrá la Información Personal del Usuario que sea importante, adecuada y no excesiva
			para los fines deseados y establecidos en este Aviso. De igual manera, procesará la Información
			Personal del Usuario en una forma consecuente con los fines para los cuales fue originalmente
			obtenida o para aquellos que el Usuario hubiese aprobado posteriormente.</li>
			<li>A través de la utilización de los Medios, el Usuario otorga su consentimiento expreso y por lo
			tanto autoriza a CCM para que esta última pueda realizar el Tratamiento de su Información Personal.
			Si el Usuario no autoriza o se opone al Tratamiento de la Información Personal ni los términos de
			este Aviso no deberá hacer uso de los Medios.</li>
			<li>Asimismo a través de la utilización de los Medios, el Usuario otorga su consentimiento expreso
			para que CCM pueda total o parcialmente transferir, revelar, comercializar, vender, rentar o brindar
			la Información Personal del Usuario a agencias de viajes, socios comerciales, instituciones de salud,
			instituciones financieras, empresas afiliadas y subsidiarias de CCM, así como a cualquier otro
			tercero ya sea nacional o extranjero, con la finalidad de complementar y mejorar los servicios que
			CCM ofrece. El consentimiento expreso del Usuario, también incluye el suministro o transferencia de
			los Datos Personales a autoridades competentes, tanto nacionales como extranjeras, ya sea por medio
			de orden, resolución o acuerdo con dichas autoridades.</li>
			<li>Se hace la advertencia que CCM puede utilizar "cookies" para confirmar su identificación,
			personalizar su acceso a los Medios y revisar su uso, con el solo propósito de mejorar su navegación
			y sus condiciones de funcionalidad, toda vez que mediante las citadas “cookies” no se recaban datos
			personales ni datos personales sensibles de los usuarios en términos de la Ley. Los Medios tienen
			ligas a otros sitios externos, de los cuales el contenido y políticas de privacidad no son
			responsabilidad de CCM.</li>
			<li>En caso de que exista algún cambio en este Aviso de Privacidad, se le comunicará a los Usuarios
			de la siguiente manera: (a) enviándole un correo electrónico a la cuenta que ha registrado o (b)
			publicando una nota visible en cualquiera de los Medios. CCM no será responsable si el Usuario no
			recibe la notificación de cambio en el Aviso si existiere algún problema con su cuenta de correo
			electrónico, de transmisión de datos por internet o si no se tuvo acceso a los Medios por
			cualquier causa no imputable a CCM.</li>
			<li>CCM se reserva el derecho a modificar o complementar en cualquier momento este Aviso para
			adaptarlo a novedades legislativas o jurisprudenciales así como a prácticas de la industria. En
			dichos supuestos, CCM anunciará los cambios introducidos con razonable antelación a su puesta en
			práctica y en la forma descrita anteriormente. No obstante, es obligación del Usuario revisar
			periódicamente este Aviso. Por otra parte, si el Usuario no ejerce su derecho de oposición, se
			mantendrá el vínculo jurídico y el Tratamiento de la Información Personal del Usuario será en
			la forma dispuesta por las nuevas disposiciones establecidas en este Aviso por CCM.</li>
			<li>Los facilitadores de protección de datos será el Lic. Oscar de la Fuente Rossano
			("Responsable").</li>
			<li>Los Usuarios tienen reconocidos y podrán ejercitar los derechos ARCO (acceso, cancelación,
			rectificación y oposición), enviando directamente su solicitud al Responsable a través de
			la cuenta de correo (datos.personalesCM@cuamoc.com). Dicha solicitud deberá contener:
			nombre y domicilio u otro medio para comunicarle la respuesta a su solicitud; los documentos
			que acrediten la identidad o, en su caso, la representación legal; la descripción clara y
			precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos
			ARCO y cualquier otro elemento que facilite la localización de los datos personales. CCM se
			compromete a realizar todo el esfuerzo razonable para cumplir con dicha solicitud. La respuesta
			a dicha solicitud será del conocimiento del Usuario. Sin perjuicio de los cambios que se
			realicen a solicitud del Usuario, CCM conservará la Información Personal anterior por motivos
			de seguridad y control del fraude. El Usuario garantiza y responde, en cualquier caso, de la
			veracidad, exactitud, vigencia y autenticidad de la Información Personal proporcionada a CCM,
			y se compromete a mantenerla debidamente actualizada.</li>
		</ol>
		<p>Las leyes y regulaciones de diferentes países pueden imponer diferentes requerimientos en el
		Internet y la protección de la información. CCM se ubica en México y todos los asuntos en
		relación a los Medios son regidos por las leyes de México, por lo que al momento de ingresar
		la información el Usuario autoriza esta transferencia y la aceptación del presente Aviso de
		Privacidad.</p>
	</div>
</div>

<?php include_once '../footer.php'; ?>