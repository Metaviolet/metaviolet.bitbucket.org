<?php
//	header("charset=utf-8");
	$id_page=1;
	include_once 'functions.php';
	include_once 'header.php';
?>

	<header id="Home" class="cd-header" data-type="slider-item">
		<h1><img src="<?=$rutaimg;?>logo.png" alt="Amstel Light" /></h1>
		<p>Resultado de un proceso sofisticado y cuidadoso, con los más finos ingredientes.</p>
		<a id="conocela-mas" class="smooth" href="#conoce-mas" title="Conoce más de Amstel Light"><span>Conócela</span><img src="<?=$rutaimg;?>flecha_abajo.png" alt="Conoce más de Amstel Light" /></a>
		<!-- <img src="<?=$rutaimg;?>h_cerveza.png" alt="La cerveza Europea Amstel Light" class="anim iz firstbeer" /> -->
	</header>
	<section id="conoce-mas" class="cd-fixed-background img-1" data-type="slider-item">
		<div class="cd-content">
			<div class="svgContainer wow slideInLeft">
				<img src="<?=$rutaimg;?>/Texto.png" alt="">
			</div>
			<a href="javascript:void(0);" class="conoce_mas" title="Conoce más de Amstel Light">Conoce más</a>
			<div class="secondbeer">
				<img src="<?=$rutaimg;?>h_cerveza_2.png" alt="La cerveza Europea Amstel Light" />
			</div>	
			<div class="conoce_mas">
				<h2><span>AMSTEL LIGHT</span> ENCIERRA UN GRAN PRIVILEGIO. CONOCE LA PUREZA DE SU SABOR EUROPEO. SINÓNIMO DE CALIDAD.</h2>
				<p>Somos la primera cerveza en Europa en utilizar una lata como envase, pioneros de lo Light. Fuimos la primer botella que, ¡oh magia! no necesitaba un destapador. 
				Siempre vamos un paso adelante, porque sabemos que sólo se puede tener un futuro prometedor haciendo honor a nuestro pasado.</p>
				<p>Desde que nació nuestra marca en 1870, hemos exportado cerveza Amstel Light a más de 120 países. Y aunque hemos crecido y cruzado miles de ríos para poder llegar a todo el mundo, cada cerveza todavía ofrece un auténtico sabor europeo. El sabor que sólo tienen aquellos que están orgullosos de sus raíces. 
				Amstel Light, con casi un siglo y medio de experiencia sirviendo la cerveza premium más fresca e innovadora, merece un lugar especial en la historia cervecera tan sólo por ser la primera cerveza en ser importada a América.</p>
				<h3>Somos Amstel Light y seguiremos haciendo historia.</h3>
				<a href="#" class="cerrar" title="Cerrar descripción">X</a>
			</div>
		</div>
		<a class="down smooth" href="#thebrew" ><img src="<?=$rutaimg;?>flecha_abajo_blanca.png" alt="Conoce más de Amstel Light" /></a>
	</section>

	<section id="thebrew" class="cd-fixed-background" data-type="slider-item">
		<a id="closeModal" href="#">X</a>
		<img class="brewtitle" src="<?=$rutaimg;?>brew/titulo_TheBrew.png" alt="">
		<div class="colLeft">
			<a id="historiaBtn" href="javascript:void(0);"><img src="<?=$rutaimg;?>brew/btn_historia.png" alt=""></a>
		</div>
		<div class="colRight">
			<a id="liquidoBtn" href="javascript:void(0);"><img src="<?=$rutaimg;?>brew/btn_liquido.png" alt=""></a>
		</div>

	<section id="escencia" class="cd-fixed-background img-4 animated">
		<div class="cd-content">
			<h2>Ingredientes</h2>
			<div class="centrar">
				<div>
					<h3>Sabor Europeo</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Sabor Europeo/</h4>
						Inspirada en el Río Amstel de Ámsterdam, fuimos la Primera Cerveza Europea Light, lo que pone de manifiesto nuestro gusto por la innovación y la sofisticación,  demostrando cómo debe saber una cerveza.
						<a href="#" class="izq" title="Cerrar descripción">X</a>
					</span>
				</div>
				<div>
					<h3>Cebada</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Cebada/</h4>
						Elegimos la cebada como el cereal ideal para elaborar nuestra cerveza y además seleccionamos solo las mejores maltas, 100% naturales, así nada puede salir mal.
						<a href="#" class="izq" title="Cerrar descripción">X</a>
					</span>
				</div>
				<div>
					<h3>Levadura</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Levadura/</h4>
						La exclusiva levadura de Amstel le aporta ese ámbar dorado y una espuma blanca intensa, cremosa y persistente en el vaso, dejando claros anillos a medida que se consume.
						<a href="#" class="der" title="Cerrar descripción">X</a>
					</span>
					
				</div>
				<div>
					<h3>Agua</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Agua/</h4>
						Si bien ya no se usa el agua directamente extraída del Río Amstel, sigue siendo fundamental en la fabricación de nuestra cerveza para lograr la mezcla perfecta que da como resultado una bebida fresca y ligera, pero con todo el sabor de Europa, para disfrutar en cualquier momento del día.
						<a href="#" class="izq" title="Cerrar descripción">X</a>
					</span>
				</div>
				<div>
					<h3>Lúpulo</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Lúpulo/</h4>
						Su aroma ligero y frutal, aflora del dulce sabor de su lúpulo y de su suave amargor.
						<a href="#" class="der" title="Cerrar descripción">X</a>
					</span>
					
				</div>
			</div>
		</div>
	</section><!--#esencia-->
	<section id="historia" class="cd-fixed-background img-3">
		<div class="cd-content">
			<h2 class="hist">Historia</h2>
			<div class="sliderHistoria">
				<div>
					<div class="blu">	
					<img src="<?=$rutaimg;?>historia/FECHAS/2016.png" alt="2016" />
					<h4>Amstel Light llega a México, con todo el sabor de Ámsterdam. </h4>
					</div>
				</div><!--2016-->
				<div>
					<div class="whi smalltextone">
						<h4>Nace Amstel light</h4>
						<p>teniendo el título de la primera cerveza europea light.</p>
						<img src="<?=$rutaimg;?>historia/FECHAS/ano_b.png" alt="1981" />
					</div><!--1981-->
				</div>
				<div>
					<div class="blu onlytext">
						<h4>Con casi un siglo y medio de experiencia </h4>
						<h3>sirviendo la cerveza premium más fresca e innovadora</h3>
						<p> Amstel se merece un lugar especial en la historia cervecera por ser la primera cerveza en ser importada a América</p>
					</div>
				</div><!--blusolotexto-->
				<div>
					<div class="whi smalltext">
						<h4>Desde que nació nuestra marca en 1870, hemos exportado cerveza Amstel a más de 120 países </h4>	
						<p>y aunque hemos crecido y cruzado miles de ríos para poder llegar a todo el mundo, cada cerveza ofrece el auténtico sabor europeo; el sabor que sólo tienen aquellos que están orgullosos de sus raíces.</p>
						<img src="<?=$rutaimg;?>historia/FECHAS/1870.png" alt="1870" />	
						
					</div>
				</div><!--1970-->
				<div>
					<div class="blu">
					<img src="<?=$rutaimg;?>historia/FECHAS/ano_a.png" alt="1870" />	
					<p>Amstel es una marca global con presencia en más de 50 países en el mundo, cuyo nombre surge  en honor al Río Amstel en Ámsterdam, en donde las personas enfriaban sus cervezas.</p>
					</div>
				</div><!--1870-->
			</div>
		</div>
	</section>
	</section><!--#thebrew-->

	<section id="hostel" class="cd-fixed-background hostel" data-type="slider-item">
		<div class="cd-content">
			<div class="colLeft">
				<img src="<?=$rutaimg;?>hostel/HostelAmstel.png" alt="">
			</div>
			<div class="colRight">
				<div class="texto">
					<h3>Hostel Amstel <br><strong>es la embajada <br>de Amsterdam en México</strong></h3>

<p>Un espacio que celebra la diversidad de una cultura siempre propositiva.</p>

<p>Es la aventura del backpacking, el<span> Amsterdam Light Festival, el Red Light District, el Flying Pig y el Bulldog Coffeeshop</span> en un mismo lugar.</p>

<p>Te invitamos a vivir la experiencia Hostel Amstel a través de una serie de eventos únicos, en los que sentirás el lado progresivo de Ámsterdam. </p>
				</div>
			</div>
		</div>
	</section>
	<section id="instagram" class="cd-fixed-background img-2" data-type="slider-item">
		<div class="cd-content">
			<img class="insta" src="<?=$rutaimg;?>lifestyle/Lifestyle.png">
	        <!-- instagram pics -->
	        <div id="instafeed"></div><!--instagram feed-->
		</div>
		<footer class="proximamente">
				<span class="nc" role="navigation">
					<a href="<?=$ruta?>aviso-de-privacidad/" target="_blank" title="Abrir el Aviso de privacidad">Aviso de privacidad</a>
					<a href="<?=$ruta?>terminos-y-condiciones/" target="_blank" title="Abrir los Términos y condiciones">Términos y condiciones</a>
					<a href="http://cuamoc.com/" target="_blank" title="Abrir Cuauhtémoc Moctezuma">&copy;2016 Cuauhtémoc Moctezuma</a>
					<a class="none" title="Cofepris">153300201A1933</a>
					<a class="none" title="Evita el exceso">EVITA EL EXCESO</a>
				</span>
				<span class="nx">
					<a href="http://www.heineken.com/mx/home.aspx" target="_blank" title="Heineken"><img src="<?=$rutaimg;?>hkm.png" alt="Heineken" /></a>
					<a href="https://www.facebook.com/ConsumoInteligenteMX" target="_blank" title="Consumo inteligente"><img src="<?=$rutaimg;?>cmoderado.png" alt="Consumo inteligente" /></a>
				</span>
			</footer>
	</section>
<?php include_once 'footer.php'; ?>