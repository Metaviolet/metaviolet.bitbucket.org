<?php
//	header("charset=utf-8");
	$id_page=1;
	include_once 'functions.php';
	include_once 'header.php';
?>

	<header id="Home" class="cd-header" data-type="slider-item">
		<h1><img src="<?=$rutaimg;?>logo.png" alt="Amstel Light" /></h1>
		<p>Resultado de un proceso sofisticado y cuidadoso, con los más finos ingredientes.</p>
		<a id="conocela-mas" class="smooth" href="#conoce-mas" title="Conoce más de Amstel Light"><span>Conócela</span><img src="<?=$rutaimg;?>flecha_abajo.png" alt="Conoce más de Amstel Light" /></a>
		<!-- <img src="<?=$rutaimg;?>h_cerveza.png" alt="La cerveza Europea Amstel Light" class="anim iz firstbeer" /> -->
	</header>

	<section id="conoce-mas" class="cd-fixed-background img-1" data-type="slider-item">
		<div class="cd-content">
			<div class="svgContainer wow slideInLeft">
				<img src="<?=$rutaimg;?>/Texto.png" alt="">
			</div>
			<a href="#" class="conoce_mas" title="Conoce más de Amstel Light">Conoce más</a>
			<div class="secondbeer">
				<img src="<?=$rutaimg;?>h_cerveza_2.png" alt="La cerveza Europea Amstel Light" />
			</div>	
			<div class="conoce_mas">
				<h2><span>AMSTEL LIGHT</span> ENCIERRA UN GRAN PRIVILEGIO. CONOCE LA PUREZA DE SU SABOR EUROPEO. SINÓNIMO DE CALIDAD.</h2>
				<p>Somos la primera cerveza en Europa en utilizar una lata como envase, pioneros de lo Light. Fuimos la primer botella que, ¡oh magia! no necesitaba un destapador. 
				Siempre vamos un paso adelante, porque sabemos que sólo se puede tener un futuro prometedor haciendo honor a nuestro pasado.</p>
				<p>Desde que nació nuestra marca en 1870, hemos exportado cerveza Amstel Light a más de 120 países. Y aunque hemos crecido y cruzado miles de ríos para poder llegar a todo el mundo, cada cerveza todavía ofrece un auténtico sabor europeo. El sabor que sólo tienen aquellos que están orgullosos de sus raíces. 
				Amstel Light, con casi un siglo y medio de experiencia sirviendo la cerveza premium más fresca e innovadora, merece un lugar especial en la historia cervecera tan sólo por ser la primera cerveza en ser importada a América.</p>
				<h3>Somos Amstel Light y seguiremos haciendo historia.</h3>
				<a href="#" class="cerrar" title="Cerrar descripción">X</a>
			</div>
		</div>
		<a class="down smooth" href="#conocela" ><img src="<?=$rutaimg;?>flecha_abajo_blanca.png" alt="Conoce más de Amstel Light" /></a>
	</section>

	<section id="conocela" class="cd-fixed-background img-2" data-type="slider-item">
		<div class="cd-content light-background">
			<h2>The Brew</h2>
			<div class="colLeft">
				<h3>Historia</h3>
			</div>
			<div class="colRight">
				<h3>Ingredientes</h3>
			</div>
		</div>
	</section>

<!-- 	<section id="historia" class="cd-fixed-background img-3" data-type="slider-item">
		<div class="cd-content">
			<h2 class="hist">Historia</h2>
			<div class="sliderHistoria">
				<div>
					<div class="blu">
						<img src="<?=$rutaimg;?>ano_a.png" alt="1870" />
						<p>Charles de Pesters y Johannes van Marwijk-Kooij elaboran una
							nueva cerveza basada en una receta única:</p>
						<h3>Amstel</h3>
					</div>
				</div>
				<div>
					<div class="whi">
						<h3>Los primeros</h3>
						<h4>Nace Amstel Light</h4>
						<p>Somos la primera cerveza en Europa en utilizar la lata como envase;
							también somos pioneros de lo Light y fuimos la primera botella
							que, ¡oh, magia!<br />No necesitaba destapador.</p>
						<img src="<?=$rutaimg;?>ano_b.png" alt="1981" />
					</div>
				</div>
				<div>
					<div class="blu">
					<h4>Desde que nació nuestra familia en 1870, 
hemos exportado Amstel a más de 120 países</h4>	
					<p>Y aunque hemos crecido y cruzado ríos para poder llegar a todo el mundo, 
cada cerveza todavía ofrece un auténtico sabor de su tierra natal. Lideramos 
la revolución de la</p>
					<img src="<?=$rutaimg;?>1870.png" alt="1870" />
						
					</div>
				</div>
				<div>
					<div class="whi smalltext">
						<img src="<?=$rutaimg;?>1970.png" alt="1970" />
						<p> En 1907 llegamos a distribuir 20 millones de litros de cerveza ese año y para 1933 nos embarcamos hacia Estados Unidos. Solo siete años después nació el primer barril Amstel portátil, al que le siguieron un sinfín de innovaciones. <strong>Pero, sin duda, uno de los grandes hitos en nuestra historia fue la unión con Heineken en 1970, la cual culminó con ¡una gran boda holandesa!</strong></p>
						<h4>Hoy, nuestra cerveza se disfruta en 60 países alrededor del mundo y seguimos haciendo historia</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="escencia" class="cd-fixed-background img-4" data-type="slider-item">
		<div class="cd-content">
			<h2>Ingredientes</h2>
			<div class="centrar">
				<div>
					<h3>Sabor Europeo</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Sabor Europeo/</h4>
						Inspirada en el río Amstel (Amsterdam), somos la primera cerveza
						light del mundo, lo que pone de manifiesto nuestro gusto por la
						innovación y la sofisticación, rescatando todo el sabor de la Europa
						moderna en una sola botella irresistible.
						<a href="#" class="izq" title="Cerrar descripción">X</a>
					</span>
				</div>
				<div>
					<h3>Cebada</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Cebada/</h4>
						Elegimos la cebada como el cereal ideal para elaborar nuestra cerveza
						y además seleccionamos solo las mejores maltas tostadas 100% naturales,
						así nada puede salir mal. Su aroma ligero frutal, aflora del sabor dulce
						de la malta Pilsen y su amargor suave.
						<a href="#" class="izq" title="Cerrar descripción">X</a>
					</span>
				</div>
				<div>
					<h3>Levadura</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Levadura/</h4>
						La exclusiva levadura de Amstel, le aporta ese ámbar dorado claro y una
						espuma blanca intensa, cremosa y persistente en el vaso, dejando claros
						anillos a medida que se consume.
						<a href="#" class="der" title="Cerrar descripción">X</a>
					</span>
					
				</div>
				<div>
					<h3>Agua</h3>
					<div class="fondo"></div>
					<span class="desc">
						<h4>Agua/</h4>
						Elemento fundamental en la fabricación de nuestra cerveza para lograr
						la mezcla perfecta que da como resultado una bebida fresca y ligera,
						pero con todo el sabor de Europa, para disfrutar en cualquier momento
						del día.
						<a href="#" class="der" title="Cerrar descripción">X</a>
					</span>
				</div>
			</div>
		</div>
	</section> -->
	<section id="instagramFeed" class="cd-fixed-background img-2" data-type="slider-item">
		<div class="cd-content light-background">
			<h2>The Brew</h2>
			<div class="colLeft">
				<h2>Historia</h2>
			</div>
			<div class="colRight">
				<h2>Ingredientes</h2>
			</div>
		</div>
	</section>

	<section id="hostelAmstel" class="cd-fixed-background img-2" data-type="slider-item">
		<div class="cd-content">
			<div class="colLeft">
				<img src="<?=$rutaimg;?>logo.png" alt="">
			</div>
			<div class="colRight">
				<div class="texto">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices mattis volutpat. Quisque fringilla ante ligula, quis cursus enim commodo sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris nisi turpis, mattis sagittis tincidunt in, tristique et metus. Duis auctor hendrerit eros, eu sagittis justo luctus in. Cras ut molestie felis. Mauris eget urna semper, luctus leo ut, vehicula nisl. Quisque cursus lorem vitae ante convallis</p>
				</div>
			</div>
		</div>
	</section>

<!-- La sección patrocinios se oculta temporalmente a petición del cliente -->
	<!-- <section style="display: none" id="patrocinios" class="cd-fixed-background img-5" data-type="slider-item">
		<div class="camisax"></div>
		<div class="cd-content">
			<img src="<?=$rutaimg;?>logo-abierto.png" alt="Abierto Monterrey"/>
			<img src="<?=$rutaimg;?>logo.png" alt="Amstel Light" />
			<img class="abierto wow slideInLeft" src="<?=$rutaimg;?>abierto_tenis.png" alt="Abierto de Tennis en Monterrey" />
			<p><span>Del 26 al 31 de octubre</span> disfruta de lo mejor del deporte blanco en Monterrey.
			El abierto de tenis reúne a los mejores tenistas del mundo en la cancha y tú podrás ser
			testigo de los mejores encuentros disfrutando de <span>Amstel Light</span> y su original
			sabor europeo.</p>
			<a class="mas wow slideInLeft" href="#">Lee más</a>
		</div>
	</section> -->
	<nav id="nu">
		<ul class="cd-vertical-nav">
			<li><a href="#0" class="cd-prev inactive">Next</a></li>
			<li><a href="#0" class="cd-next">Prev</a></li>
		</ul>
	</nav> 

<?php include_once 'footer.php'; ?>