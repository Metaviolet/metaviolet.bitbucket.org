<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<title>Amstel Light</title>
	<link href="<?=$ruta;?>css/style.css" type="text/css" rel="stylesheet" media="screen"/>
	<link href="<?=$ruta;?>css/newstyles.css" type="text/css" rel="stylesheet" media="screen"/>
	<?php if($id_page==2){ ?>
		<link href="<?=$ruta;?>css/contador.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<?php } ?>
	<link rel="icon" href="<?=$ruta;?>favicon.ico" type="image/x-icon" />
	<meta name="description" content="<?=$descripcion;?>" />
	<meta name="keywords" content="Amstel Light,Cerveza Light,Amstel,Heineken" />
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="<?=$ruta;?>js/modernizr.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" charset="UTF-8"></script>
	<?php if($id_page==2){ ?>
		<script src="<?=$ruta;?>js/flipclock.js" charset="utf-8"></script>
	<?php } ?>

	<?php if($id_page==1){ ?>
	<link rel="stylesheet" href="css/animate.css">
	<script src="bower_components/wow/dist/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <link rel="stylesheet" href="<?=$ruta;?>js/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="<?=$ruta;?>js/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="<?=$ruta;?>css/instagram-feed-styles.css">
    <script src="<?=$ruta;?>js/slick-carousel/slick/slick.min.js"></script>
    <!--<script type="text/javascript" src="<?=$ruta;?>js/instagram-feed.js"></script>-->
    <?php } ?>
	    <script type="text/javascript" src="<?=$ruta;?>node_modules/instafeed.js/instafeed.min.js"></script>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<link rel="apple-touch-icon" href="<?=$rutaimg;?>touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?=$rutaimg;?>touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?=$rutaimg;?>touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?=$rutaimg;?>touch-icon-ipad-retina.png" />
	<link rel="apple-touch-startup-image" href="<?=$rutaimg;?>touch-icon-ipad-retina.png" />

	<meta name="application-name" content="Amstel Light" />
	<meta name="msapplication-TileColor" content="#5195d1" />
	<meta name="msapplication-square70x70logo" content="<?=$rutaimg;?>tiny.png" />
	<meta name="msapplication-square150x150logo" content="<?=$rutaimg;?>square.png" />
	<meta name="msapplication-wide310x150logo" content="<?=$rutaimg;?>wide.png" />
	<meta name="msapplication-square310x310logo" content="<?=$rutaimg;?>large.png" />

	<meta property="og:title" content="Próximamente, Amstel Light" />
	<meta property="og:site_name" content="Amstel Light" />
	<meta property="og:description" content="Próximamente Amstel Light" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=$ruta;?>" />
	<meta property="og:image" content="<?=$rutaimg;?>logos.png" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:locale:alternate" content="es_ES" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@AmstelLight" />
	<meta name="twitter:title" content="Próximamente, Amstel Light" />
	<meta name="twitter:description" content="Próximamente, Amstel Light" />
	<meta name="twitter:image" content="<?=$rutaimg;?>large.png" />

</head>
<body class="<?=$clase;?>">
<div class="cargando"></div>
<div class="bloqueo"></div>
<div class="menu_grande cerrado">
	<ul>
		<li>
			<span></span>
			<a class="smooth" href="#Home">Home</a>
			<span></span>
		</li>
		<li>
			<span></span>
			<a class="smooth" href="#conoce-mas">Spirit of Amsterdam</a>
			<span></span>
		</li>
		<li>
			<span></span>
			<a class="smooth" href="#thebrew">The Brew</a>
			<span></span>
		</li>
		<li>
			<span></span>
			<a class="smooth" href="#hostel">Hostel Amstel</a>
			<span></span>
		</li>
		<li>
			<span></span>
			<a class="smooth" href="#instafeed">Lifestyle</a>
			<span></span>
		</li>
		<li><a id="cierra_mnu_grande" href="javascript:void(0);">X</a></li>
	</ul>
</div>
<div id="simplemodal-data">
	<p>Bienvenido</p>
	<img src="<?=$rutaimg;?>logo_new.png" alt="Amstel Light" />
	<div id="spth" class="body">
		<h2>¿Quieres conocer tu lado europeo?</h2>
		<h4>Compártenos tu fecha de nacimiento.</h4>
		<form id="fechaNacimiento" class="checkAgeForm">
			<div class="inputs">
				<ul>
					<li><input id="day" type="number" type="numeric" pattern="[0-9]*" inputmode="numeric" class="dateField" placeholder="DD" value="" maxlength="2" name="day" autocomplete="off"></li>
					<li><input id="month" type="number" type="numeric" pattern="[0-9]*" inputmode="numeric" class="dateField" placeholder="MM" value="" maxlength="2" name="month" autocomplete="off"></li>
					<li><input id="year" type="number" type="numeric" pattern="[0-9]*" inputmode="numeric" class="dateField large" placeholder="AA" value="" maxlength="2" name="year" autocomplete="off"></li>
				</ul>
			</div>
			<div class="selects">
				<span class="lt-select-wrapper"><select class="styled"></select></span>
				<button type="submit">ENTRAR</button>
			</div>
			<a class="im" href="http://cuamoc.com/" target="_blank"><img src="<?=$rutaimg;?>hkm.png" alt="Heineken" width="31" height="40" /></a>
			<a class="im" href="https://www.facebook.com/ConsumoInteligenteMX" target="_blank"><img src="<?=$rutaimg;?>cmoderado.png" alt="Consumo inteligente" width="25" height="40" /></a>
			<p>DERECHOS RESERVADOS &copy;2016. CUAUHTÉMOC MOCTEZUMA.<br />
				El acceso y uso de este sitio esta condicionado a <a href="#" target="_blank">Políticas de Privacidad</a><br />
				e Información Legal</p>
		</form>
	</div>
</div><!--simplemodal-->
<?php if($id_page==1){ ?>
<nav id="menuHorizontal">
	<ul>
		<li><img src="<?=$rutaimg;?>logo.png"</li>
		<li><span></span><a class="smooth" href="#Home">Home</a><span></span></li>
		<li><span></span><a class="smooth" href="#conoce-mas">Spirit of amsterdam</a><span></span></li>
		<li><span></span><a class="smooth" href="#thebrew">The Brew</a><span></span></li>
		<li><span></span><a class="smooth" href="#hostel">Hostel Amstel</a><span></span></li>
		<li><span></span><a class="smooth" href="#instafeed">Lifestyle</a><span></span></li>
		<li class="social"><a id="facebook" target="_blank" href="http://www.facebook.com/AmstelLightMx"></a></li>
		<li class="social"><a id="twitter" target="_blank" href="https://twitter.com/AmstelLightMx"></a></li>
		<li class="social"><a id="instagram" target="_blank" href="https://instagram.com/amstellightmx/"></a></li>
	</ul>
</nav>
	<a id="mnu_principal" href="#" class="menu"><i class="fa fa-bars"></i></a>
<?php } ?>
