var clock,fondo=1;

$(document).ready(function(){

	function nextSection(){
		if (!animating){
			if($('.is-visible[data-type="slider-item"]').next().length > 0) smoothScroll($('.is-visible[data-type="slider-item"]').next());
		}
	}
	function prevSection(){
		if (!animating){
			var prevSection = $('.is-visible[data-type="slider-item"]');
			if(prevSection.length > 0 && $(window).scrollTop() != prevSection.offset().top) {
				smoothScroll(prevSection);
			} else if(prevSection.prev().length > 0 && $(window).scrollTop() == prevSection.offset().top) {
				smoothScroll(prevSection.prev('[data-type="slider-item"]'));
			}
		}
	}
	function setSlider(){
		checkNavigation();
		checkVisibleSection();
	}
	function checkNavigation(){
		( $(window).scrollTop() < $(window).height()/2 ) ? $('.cd-vertical-nav .cd-prev').addClass('inactive') : $('.cd-vertical-nav .cd-prev').removeClass('inactive');
		( $(window).scrollTop() > $(document).height() - 3*$(window).height()/2 ) ? $('.cd-vertical-nav .cd-next').addClass('inactive') : $('.cd-vertical-nav .cd-next').removeClass('inactive');
	}
	function checkVisibleSection() {
		var scrollTop = $(window).scrollTop(),
			windowHeight = $(window).height();

		$('[data-type="slider-item"]').each(function(){
			var actualBlock = $(this),
				offset = scrollTop - actualBlock.offset().top + 100;
			( offset >= 0 && offset < windowHeight ) ? actualBlock.addClass('is-visible') : actualBlock.removeClass('is-visible');
		});
	}
	function smoothScroll(target){
		animating = true;
        $('body,html').animate({'scrollTop': target.offset().top}, 500, function(){ animating = false; });
	}

	if($(".contador").length){
		var clock,
			cur=new Date(),
			fut=new Date(2015,10,1,0,0,0,0); // 8=Septiembre
		var dif=fut.getTime()/1000 - cur.getTime()/1000;
		clock = $('.clock').FlipClock({
			clockFace:'DailyCounter',
			autoStart:false,
			callbacks:{
				stop: function(){
					$('.message').html('Comenzamos!')
				}
			}
		});
		clock.setTime(dif);
		clock.setCountdown(true);
		clock.start();
	}

	var ancho_pantalla=$(window).width();

	if($(".sliderHistoria").length){
		if(ancho_pantalla<767){
			var sliderHistoria = $(".sliderHistoria").sudoSlider({slideCount:1,touch:true,mouseTouch:true,numeric:false,prevNext:true});
		}
		else if(ancho_pantalla<1440){
			var sliderHistoria = $(".sliderHistoria").sudoSlider({slideCount:2,touch:true,mouseTouch:true,numeric:false,prevNext:true});
		}
		else{
			var sliderHistoria = $(".sliderHistoria").sudoSlider({slideCount:3,touch:true,mouseTouch:true,numeric:false,prevNext:true});
		}
	}

	if($(".cd-header").length){

		var animating = false;
		setSlider();

		$(window).on('scroll resize', function(){

			ancho_pantalla=$(window).width();

			(!window.requestAnimationFrame) ? setSlider() : window.requestAnimationFrame(setSlider);

			var classHeader	=$('.cd-header').attr('class'),
				img1 		=$('.cd-fixed-background.img-1').attr('class');
				img2		=$('.cd-fixed-background.img-2').attr('class');
				img3		=$('.cd-fixed-background.img-3').attr('class');



//			var neon = classHeader.indexOf("12213");

			if(classHeader=='cd-header is-visible'){
				$('a.menu').removeClass('negro');
				$('#facebook, #twitter, #instagram').removeClass('dark');
//				$('nav#nu').hide();
			}
			else if(img3=='cd-fixed-background img-3 is-visible'){
				$('a.menu').addClass('negro');
				$('#facebook, #twitter, #instagram').addClass('dark');
			}
			else{
				$('a.menu').removeClass('negro')
				$('#facebook, #twitter, #instagram').removeClass('dark');
//				$('nav#nu').show();
			}

			/*----------  Imagen cerveza inicial  ----------*/



			/*----------  Imagen conoce tu lado europeo  ----------*/
			
			// if(img1=='cd-fixed-background img-1 is-visible'){
			// 	$('.secondbeer').animate({right: '2%'}, 500);
			// 	$('.svgContainer').animate({left: '5%'}, 500);
			// }
			// else{
			// 	$('.secondbeer').css('right', '-40%');
			// 	$('.svgContainer').animate({left: '-50%'}, 500);
			// }

			/*----------  Imagen ingredientes  ----------*/


			

			if(ancho_pantalla<767){
				sliderHistoria.destroy();
				sliderHistoria.sudoSlider({slideCount:1,touch:true,mouseTouch:true,numeric:false,prevNext:true});
				sliderHistoria.init();
			}
			else if(ancho_pantalla<1440){
				sliderHistoria.destroy();
				sliderHistoria.sudoSlider({slideCount:2,touch:true,mouseTouch:true,numeric:false,prevNext:true});
				sliderHistoria.init();
			}
			else{
				sliderHistoria.destroy();
				sliderHistoria.sudoSlider({slideCount:3,touch:true,mouseTouch:true,numeric:false,prevNext:true});
				sliderHistoria.init();
			}

		});

	}

		/*----------  Smooth scroll para secciones  ----------*/

		$('a.smooth').on('click', function(e) {
			e.preventDefault();
			var $link = $(this);
			var anchor = $link.attr('href');
			$('html, body').stop().animate({
			scrollTop: $(anchor).offset().top
			}, 800);
			});

	    $('.cd-vertical-nav .cd-prev').on('click', function(){
	    	prevSection();
	    });
	    $('.cd-vertical-nav .cd-next').on('click', function(){
	    	nextSection();
	    });

		$(window).scroll(function(){ // 76 es la medida del footer
			var x=document.body.scrollHeight-$(this).scrollTop()-90,
				y=$(this).height();
			if(x<=y){
				$('body .cd-fixed-background.img-5 .camisax').fadeIn(3500);
			}
/*			else{
				$('body .cd-fixed-background.img-5 .camisax').fadeOut(800);
			} */
		});

		/*----------  Apertura y cierre de bloque de texto en 'Conoce mas'  ----------*/

		$('body .cd-fixed-background.img-1 .cd-content a.conoce_mas').click(function(event){
			event.preventDefault();
			$('body .cd-fixed-background.img-1').addClass('mas');
			$('body .cd-fixed-background.img-1 .cd-content a.conoce_mas').hide();
			$('body .cd-fixed-background.img-1 .cd-content .svgContainer').animate({left : '-100%', opacity: 0},800);
		});

		$('body .cd-fixed-background.img-1 .cd-content div.conoce_mas a.cerrar').click(function(event){
			event.preventDefault();
			$('body .cd-fixed-background.img-1').removeClass('mas');
			$('body .cd-fixed-background.img-1 .cd-content .svgContainer').animate({left : '7%', opacity :1},800);
			setTimeout(function(){
				$('body .cd-fixed-background.img-1 .cd-content a.conoce_mas').show();
			},800);
		});

/*----------  Apertura y cierre de bloques de texto en 'Ingredientes' ----------*/
		
		var ing_abierto=false;
		$('body .cd-fixed-background.img-4 .cd-content div div').click(function(){
			if(ing_abierto == false){
				$('body .cd-fixed-background.img-4 .cd-content div div').not(".fondo").removeClass('abierto').addClass('cerrados');
				$(this).removeClass('cerrados').addClass('abierto');
				$(this).find('.desc').addClass('animate fadeInRightBig ');
				ing_abierto=true;
				return ing_abierto;
				}
			//else{
			//}
		});

			$('a.izq, a.der').on('click', function(event) {
				if(ing_abierto == true){
					//event.preventDefault();
					/* Act on the event */
					$('body .cd-fixed-background.img-4 .cd-content div div.cerrados').removeClass('cerrados');
					$(this).parent().removeClass('animate fadeInRightBig ');
					$(this).parent().parent().removeClass('abierto').removeClass('cerrados');
					}
					ing_abierto=false;
					return ing_abierto;
				});

		$('body .cd-fixed-background.img-5 a.mas').click(function(event){
			event.preventDefault();
			$('body .cd-fixed-background.img-5 .cd-content').addClass('leemas');
		});
	//}

/*----------  Acciones del menú principal  ----------*/

	if($("a#mnu_principal").length){
		var mnu_abierto=false;
		$('#mnu_principal').click(function(event){
			event.preventDefault();
			if(mnu_abierto){
				$('body .menu_grande').addClass('cerrado');
				mnu_abierto=false;
			}
			else{
				$('body .menu_grande').removeClass('cerrado');
				mnu_abierto=true;
			}
		});
		$('.menu_grande ul li a').click(function(event){
			if($(event.target).is("#cierra_mnu_grande")){
				event.preventDefault();
			}
			$('body .menu_grande').addClass('cerrado');
			mnu_abierto=false;
		});
	}



    //  var feed = new Instafeed({
    //     get: 'user',
    //     userId: '1964370282',
    //     accessToken: '09be6eb44c4341cc95020b7275b643d4',
    //     target: 'instafeed',
    //     /*filter: function(image) {
    //         return image.tags.indexOf('AmstelLight') >= 0;
    //     }*/
    // });
    // feed.run();


    var feed = new Instafeed({
        get: 'user',
    	userId: '718015246',
        target: 'instafeed',
        clientId: 'd1998f5506584087b577548add0d37e0',
        accessToken: '718015246.d1998f5.4b20037ba9d2426fb7adbaa3a5f19517',
        resolution: 'thumbnail',
        template:"<div class='instagram-wrap crsl-item'><a target='_blank' href='{{link}}'><img class='instagram-image' src='{{image}}' /></a></div>"
    	});
    feed.run();

});

/*----------  Animaciones al cargar la pagina  ----------*/


// function removeAnim(){
// 	$(this).removeClass('animated slideInLeft');
// }

// $(window).load(function(){
// 	$('body .wow, .firstbeer').css('visibility', 'hidden');
// 	setTimeout(function(){
// 				$('body').toggleClass('loaded');
//                 $('.firstbeer').addClass('animated slideInLeft');
//             },2000);
// 	$('.firstbeer').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', removeAnim);
		
// 	});

/*========================================================
=            Script nuevos Ajustes Marzo 2016            =
========================================================*/

$('#historiaBtn').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('#historia').animate({right: 0}, 400).addClass('activated');
	$('#closeModal').animate({opacity:1}, 500);
});

$('#liquidoBtn').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('#escencia').animate({left: 0}, 400).addClass('activated');
	$('#closeModal').animate({opacity:1}, 500);
});

$('#thebrew #closeModal').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('#historia.activated').animate({right: '100%'}, 400).removeClass('activated');
	$('#escencia.activated').animate({left: '100%'}, 400).removeClass('activated');
	$(this).animate({opacity:0}, 200);

});

$(window).load(function() {
$('#instafeed').slick({
  slidesToShow: 5,
  //slidesToScroll: 1,
  //autoplay: true,
  //autoplaySpeed: 1000,
  arrows : true,
  autoplay: true,
  autoplaySpeed: 1000,
  draggable: false,
  //speed: 200,
  swipe: true,
  swipeToSlide: true,
  infinite: true,
  slidesToScroll: 5,
  prevArrow: '<button type="button" class="slick-prev">Previous</button>',
  nextArrow: '<button type="button" class="slick-next">next</button>',
  appendArrows: '#instafeed',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]

});

});

