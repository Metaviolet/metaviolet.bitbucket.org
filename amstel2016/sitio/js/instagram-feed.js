jQuery(document).ready(function($) {
	
////////////////////////////////
//
// CONFIGURATION VARIABLES http://stuffthatspins.com/2014/03/11/display-instagram-hashtag-and-user-picture-stream-really-easy-with-jquery-and-json/
//
////////////////////////////////

//http://www.amstellight.mx/proximamente/#access_token=718015246.d1998f5.4b20037ba9d2426fb7adbaa3a5f19517//*** YOU NEED TO GET YOUR OWN ACCESS TOKEN FROM INSTAGRAM
var access_token = "718015246.d1998f5.4b20037ba9d2426fb7adbaa3a5f19517";
//http://instagram.com/developer/authentication/
//http://dmolsen.com/2013/04/05/generating-access-tokens-for-instagram/

var resolution = "thumbnail"; // resolution: low_resolution, thumbnail, standard_resolution
var user_id = ""; //userid
var hashtag = "beer"; // #hashtag
var last_url = "";

//HASHTAG URL - USE THIS URL FOR HASHTAG PICS
var start_url = "https://api.instagram.com/v1/tags/"+hashtag+"/media/recent/?access_token="+access_token;
//USER URL - USE THIS URL FOR USER PICS
//var start_url = "https://api.instagram.com/v1/users/"+user_id+"/media/recent/?access_token="+access_token;

//https://api.instagram.com/v1/tags/racehungry/media/recent?access_token=1836…6303057241113856435_1395676110362&_=1395676128688&max_tag_id=1343521624608

//CALL THE SCRIPT TO START...

	//APPEND LOAD MORE BUTTON TO THE BODY...
	$("#more" ).click(function() {  
		var next_url = $(this).attr('nexturl');
		loadEmUp(next_url);
		return false;
	});

	//start your engines
	loadEmUp(start_url);


function loadEmUp(next_url){

	console.log("loadEmUp url:" + next_url);
	url = next_url;
	
	$(function() {
	    $.ajax({
		    	type: "GET",
		        dataType: "jsonp",
		        cache: false,
		        url: url ,
		        success: function(data) {
		        console.log("data: " + JSON.stringify(data) );
		        
		  		next_url = data.pagination.next_url;
		  		count = data.data.length;
		  		//three rows of four
		  		count = 20; 
		
		  		//uncommment to see da codez
		       	console.log("count: " + count );
		        console.log("next_url: " + next_url );
				console.log("data: " + JSON.stringify(data) );
				
	            for (var i = 0; i < count; i++) {
						if (typeof data.data[i] !== 'undefined' ) {
						//console.log("id: " + data.data[i].id);
							$("#instagram").append("<div class='instagram-wrap' id='pic-"+ data.data[i].id +"' ><a target='_blank' href='" + data.data[i].link +"'><span class='likes'>"+data.data[i].likes.count +"</span><img class='instagram-image' src='" + data.data[i].images.low_resolution.url +"' /></a></div>"
						);  
						}  
	      		}     
		  			  	
		  		console.log("next_url: " + next_url);
		  		$("#showMore").hide();
		  		if (typeof next_url === 'undefined' || next_url.length < 10 ) {
			  		console.log("NO MORE");
			  		$("#showMore").hide();
			  		$( "#more" ).attr( "next_url", "");
		  		}
		  		
		  		
	      		else {
			        //set button value
			        console.log("displaying more");
			        $("#showMore").show();
			        $( "#more" ).attr( "next_url", next_url);
			        last_url = next_url;
		      		
	      		}
	        }
	    });
	});
}

	
});

$(window).load(function() {
// 	// CALL CAROUSEL
$('#instagram').slick({
  slidesToShow: 5,
  //slidesToScroll: 1,
  //autoplay: true,
  //autoplaySpeed: 1000,
  arrows : true,
  autoplay: true,
  autoplaySpeed: 1000,
  draggable: false,
  //speed: 200,
  swipe: true,
  swipeToSlide: true,
  infinite: true,
  slidesToScroll: 5,
  prevArrow: '<button type="button" class="slick-prev">Previous</button>',
  nextArrow: '<button type="button" class="slick-next">next</button>',
  appendArrows: '#instagram',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]

});
// 	$('.crsl-items').carousel({ speed: 1000, autoRotate: 3000, overflow: true, visible: 4, itemMinWidth: 320, itemMargin: 0 });

 });