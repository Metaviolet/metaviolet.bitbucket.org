$(document).ready(function(){
	var url = "/easydb/procesa.php";
	$("#formmv").submit(function(event){
		event.preventDefault();
		sendData($(this));
	});
	$("#formdk").submit(function(event){
		event.preventDefault();
		sendData($(this));
	});
	function sendData(formulario){
		var happyData = formulario.serialize();
		$.ajax({
			url: url,
			type:"POST",
			data:happyData,
			dataType: "JSON",
			success: function(respuesta){
				if(respuesta.e === false){
					$(".gracias").text('Ups, ocurrió un error al registrarte. Intentalo nuevamente por favor.');
					$(".gracias").show();
				} else {				
					$("#formmv, #formdk").hide();
					$(".gracias").text('Gracias por registrarte');
					$(".gracias").show();
				}
				console.log('ajax exitoso ' + respuesta);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				$(".gracias").text('Ups, ocurrió un error de comunicación. Intentalo nuevamente por favor.');
				$(".gracias").show();
				console.log("Status: " + textStatus + "Error: " + errorThrown);
			}
		}); 
	}
});