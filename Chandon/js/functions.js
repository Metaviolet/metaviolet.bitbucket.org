/* VARIABLES PARA SLIDER'S*/
var slider_height = new Array();
var screen_width = $(window).width();
var screen_height = $(window).height();
var totalpixels = screen_width;

/* SLIDER 1 (#contentitem) */
var current_slide = 1;
var total_slides = 1;
var slider_interval;

/* SLIDER 2 (#contentitem2) */
var current_slide2 = 1;
var total_slides2 = 1;
var slider_interval2;

/*----------  Cargador y detección de landscape ----------*/
$('.btn-nav').on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
});

$(window).load(function(){

	// window.location.reload();
	// console.log('reloaded');
	
	$('body').css('opacity', 1);
	console.log('works!');
	anchoventana = window.width;

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		console.log("You are in mobile browser");
		if(anchoventana < 1024 && window.innerHeight < anchoventana){
    		$('#turnPhone').fadeIn('fast');
		}
	}else{
		console.log("You are in desktop browser");
	}	
});

/*----------  Evento de cambio de orientación del teléfono  ----------*/


$( window ).on( "orientationchange", function( event ) {

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	  if(event.orientation == "landscape"){
	  	console.log('cambio a landscape');
	  	$('#turnPhone').fadeIn('fast');
	  }
	  if(event.orientation == "portrait"){
	  	console.log('cambio a postrait');
	  	$('#turnPhone').fadeOut('fast');
	  }
	}
});



/* FUNCTIONS READY*/
$(document).ready(function() {
	setTimeout(function(){
		$("div#loadingbox").fadeOut();
	},4000);

	$(function() {
     var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
     $(".btn-nav").each(function(){
          if($(this).attr("onclick") == 'location.href=' + '\''+pgurl+'\'' || $(this).attr("onclick") == '' )
          $(this).addClass("active");
     })
});

	$('body').css('opacity', 0);
/* ALTO DEL SLIDE */
	/*Animaciones de slider 1*/
	total_slides = $('.item').length;
	$('.item').css('height', '100%');

	/* Animaciones de slider 2*/
	total_slides2 = $('.item2').length;
	$('.item2').css('height', '100%');

/* ANCHO DE SLIDER */
	/*Ancho slider 1*/
	var content_width = total_slides * 100 + '%';
	var slide_width = 100 / total_slides + '%';

	$("#cont-items").css('width', content_width);
	$( ".item" ).each(function( index ) {
		$(this).css('width',slide_width);
	});

	/*Ancho slider 2*/
	var content_width2 = total_slides2 * 100 + '%';
	var slide_width2 = 100 / total_slides2 + '%';

	$("#cont-items2").css('width', content_width2);
	$( ".item2" ).each(function( index ) {
		$(this).css('width',slide_width2);
	});

/* INTERVALO DE TIEMPO SLIDE 
	slider_interval = setTimeout("moveSlide()",20000);*/
/* INTERVALO DE TIEMPO SLIDE 2 
	slider_interval2 = setTimeout("moveSlide2()",20000);*/

/* EVENTO TOUCH PARA MOBILE */
	$('#cont-items').touchSwipe(callback1);

	function callback1(direction) {
		if(direction=="left"){
			moveSlide();
		}else if(direction=="right"){
			backSlide();
		}
	}

	$('#cont-items2').touchSwipe(callback2);

	function callback2(direction) {
		if(direction=="left"){
			moveSlide2();
		}else if(direction=="right"){
			backSlide2();
		}
	}

	 //ANIMATIONS SCROLL  ACTIVAR DE NUEVO HASTA QUE PASE EL VALIDADOR.
	$(window).scroll(function() {
		// var elementPosition = $('.mod1').offset();
		// var headerPosition = $('.headerfixed.verified').offset();

		// if($(window).scrollTop() > elementPosition.top){

  //          	$('.headerfixed.verified').css('position','fixed').css('top','0');
  //       } else {
  //           $('.headerfixed.verified').css('position','static');
  //       }

		$(".mod1-herencia").animate({
	        opacity:'1'
	        
	    }, 4000, "easeOutExpo");
	    
	  $(".mod1").animate({
	        opacity:'1'
	        
	    }, 4000, "easeOutExpo");

	});
	/*----------  Temporalmente desactivado  ----------*/
	
	setTimeout(intro_fn, 1500);

	/**/
	


/*===============================================
=        FUNCIONES DEL VALIDADOR DE EDAD        =
===============================================*/

// your functions go here
		var $animation_elements = $('.animation-element');
		var $window = $(window);

		function check_if_in_view() {
		  var window_height = $window.height();
		  var window_top_position = $window.scrollTop();
		  var window_bottom_position = (window_top_position + window_height);
		 
		  $.each($animation_elements, function() {
		    var $element = $(this);
		    var element_height = $element.outerHeight();
		    var element_top_position = $element.offset().top;
		    var element_bottom_position = (element_top_position + element_height);
		 
		    //check to see if this current container is within viewport
		    if ((element_bottom_position >= window_top_position) &&
		        (element_top_position <= window_bottom_position)) {
		      $element.addClass('in-view');
		    } else {
		      $element.removeClass('in-view');
		    }
		  });
		}

/*----------  Cosas de los campos del verificador de edad  ----------*/

var heightModal = $('#ageVerify').height();
var heightHeaderLocked = $('.headerfixed.verified header.col').height();

$('body.locked').height(heightModal);


    var paises = new Array("País","Abjasia","Acrotiri y Dhekelia","Afganistán","Albania","Alemania","Andorra","Angola","Anguila","Antigua y Barbuda","Arabia Saudita","Argelia","Argentina","Armenia","Aruba","Australia","Austria","Azerbaiyán","Bahamas","Baréin","Bangladés","Barbados","Bélgica","Belice","Benín","Bermudas","Bielorrusia","Birmania","Bolivia","Bosnia y Herzegovina","Botsuana","Brasil","Brunéi","Bulgaria","Burkina Faso","Burundi","Bután","Cabo Verde","Caimán, Islas","Camboya","Camerún","Canadá","Catar","Centroafricana, Rep.","Chad","Checa, Rep.","Chile","China","Chipre","Chipre, RTN","Cocos, Islas","Colombia","Comoras","Rep. del Congo","Rep. Dem. del Congo","Cook, Islas","Corea del Norte","Corea del Sur","Costa de Marfil","Costa Rica","Croacia","Cuba","Curazao","Dinamarca","Dominica","Dominicana, Rep.","Ecuador","Egipto","El Salvador","Emiratos Árabes Unidos","Eritrea","Eslovaquia","Eslovenia","España","Estados Unidos","Estonia","Etiopía","Feroe, Islas","Filipinas","Finlandia","Fiyi","Francia","Gabón","Gambia","Georgia","Ghana","Gibraltar","Granada","Grecia","Groenlandia","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guinea Ecuatorial","Guyana","Haití","Honduras","Hong Kong","Hungría","India","Indonesia","Irak","Irán","Irlanda","Islandia","Israel","Italia","Jamaica","Japón"," Jersey","Jordania","Kazajistán","Kenia","Kirguistán","Kiribati","Kosovo","Kuwait","Laos","Lesoto","Letonia","Líbano","Liberia","Libia","Liechtenstein","Lituania","Luxemburgo","Macao","Macedonia","Madagascar","Malasia","Malaui","Maldivas","Malí","Malta"," Malvinas, Islas"," Man, Isla de"," Marianas del Norte, Islas","Marruecos"," Marshall, Islas","Mauricio","Mauritania","México","Micronesia","Moldavia","Mónaco","Mongolia","Montenegro","Montserrat","Mozambique","Nagorno Karabaj","Namibia","Nauru"," Navidad, Isla de","Nepal","Nicaragua","Níger","Nigeria","Niue"," Norfolk, Isla","Noruega","Nueva Caledonia","Nueva Zelanda","Omán","Osetia del Sur","Países Bajos","Pakistán","Palaos","Palestina","Panamá","Papúa Nueva Guinea","Paraguay","Perú","Pitcairn, Islas","Polinesia Francesa","Polonia","Portugal","Puerto Rico","Reino Unido","Ruanda","Rumania","Rusia","Sahara Occidental"," Salomón, Islas","Samoa","Samoa Americana","San Bartolomé","San Cristóbal y Nieves","San Marino","San Martín (Francia)","San Martín (Países Bajos)","San Pedro y Miquelón","San Vicente y las Granadinas","Santa Elena, Ascensión y Tristán de uña","Santa Lucía","Santo Tomé y Príncipe","Senegal","Serbia","Seychelles","Sierra Leona","Singapur","Siria","Somalia","Somalilandia","Sri Lanka","Suazilandia","Sudáfrica","Sudán","Sudán del Sur","Suecia","Suiza","Surinam","Svalbard","Tailandia","Taiwán","Tanzania","Tayikistán","Timor Oriental","Togo","Tokelau","Tonga","Transnistria","Trinidad y Tobago","Túnez","Turcas y Caicos, Islas","Turkmenistán","Turquía","Tuvalu","Ucrania","Uganda","Uruguay","Uzbekistán","Vanuatu","Vaticano, Ciudad del","Venezuela","Vietnam","Vírgenes Británicas, Islas","Vírgenes de los Estados Unidos, Islas","Wallis y Futuna","Yemen","Yibuti","Zambia","Zimbabue");
function cleanString(str){
    str = replaceAll( str, "á", "a" );
    str = replaceAll( str, "é", "e" );
    str = replaceAll( str, "í", "i" );
    str = replaceAll( str, "ó", "o" );
    str = replaceAll( str, "ú", "u" );
    str = replaceAll( str, "Á", "A" );
    str = replaceAll( str, "É", "E" );
    str = replaceAll( str, "Í", "I" );
    str = replaceAll( str, "Ó", "O" );
    str = replaceAll( str, "Ú", "U" );
    str = replaceAll( str, "ñ", "n" );
    str = replaceAll( str, "Ñ", "N" );
    return str.toLowerCase();
}
function replaceAll( text, busca, reemplaza ){
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}

        for(var i=0;i<paises.length;i++) { //No está funcionando el append
            $('select.paises').append("<option class='"+cleanString(paises[i])+"' value='"+cleanString(paises[i])+"'>"+paises[i]+"</option>");
        }

        $('select .pais').attr("selected","selected");
        $(".dateField").val("");

/*----------  Cookie  ----------*/

function setCookie(cname,cvalue,exdays){
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	}
function getCookie(cname){
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++){
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}

var cookieName="chandon-mx";
	if(getCookie(cookieName)>0){
		$("#ageVerify").hide(); //Cambiar por función que scrollea y oculta el verificador
		intro_fn();
		$('body').removeClass('locked').height('auto');
		$('.headerfixed').addClass('verified');
		$('.mainLogo').addClass('visible');
	}
	else{ //Todas estas acciones se ejecutan si no tiene la cookie
	   
		$("#fechaNacimiento").bind("submit", function (e){
	        e.preventDefault();
	        e.stopPropagation();

	         if (!isValidDate()) return false;
	        var otraURL = "http://www.conductordesignado.com.mx";
	        var pais = $("select option:selected").val();
	
	        if (!isOver18()){
	        	alert('Tienes menos de 18 años');
	            //top.location = otraURL;
			}
	
			// var day = $("#day").val();
			// var month = $("#month").val();
			// var year = $("#year").val();
			// var age = 18;
			// var mydate = new Date();
			// mydate.setFullYear(year, month-1, day);

			// var currdate = new Date();
			// currdate.setFullYear(currdate.getFullYear() - age);

			// if ((currdate - mydate) < 0){
			// 	alert("TOO YOUNG TO ROCK!!");
			// } 

			else{
				//Las magias de cuando ya pasó el verificador
				setCookie(cookieName,1,15);
				console.log('Cookie set');
				
				//$('.legales').animate({height: '100px', opacity: 0}, 100);
				$('#ageVerify').slideUp(600);

				//$('#ageVerify').animate({height : 0}, 1000, function(){
				setTimeout(function(){
					$('body').removeClass('locked').height('auto');
					$('.headerfixed').addClass('verified');
					$('.mainLogo').addClass('visible');
				},300);

				
				//$('.modalVerify').hide();				
				check_if_in_view();
				//$('#ageVerify').hide('slow');
				
				//});
				//
			}

			return false;
		});
	}
/*----------  Validación jQuery  ----------*/
 var m = "#fechaNacimiento";

function isOver18(){
   
    var fecha = new Date($(m+" #year").val(), $(m+" #month").val()*1-1, $(m+" #day").val());
    var ahora = new Date();
    var age = ahora.getTime() - fecha.getTime();

    return !(age < (1000 * 60 * 60 * 24 * 365.26 * 18));
	}

function changeInputSatus(selector,rem){
    var cl = "error"; //error class
    var d= 100; // delay
    if(typeof rem==="undefined") rem = false;

    $(selector).addClass(cl);
    if(rem) $(selector).removeClass(cl);
    return false;
}

function isValidDate(keyPressed){
    if(typeof keyPressed==="undefined") keyPressed = false;
    var m = "#fechaNacimiento ";
    $(m+" input").removeClass("error");
    var isValid = true;
    if($(m+" #day").val()=="" && $(m+" #month").val()=="" && $(m+" #year").val()=="") {
        if(!keyPressed) changeInputSatus(m+" input");
        return false
    };
    if(!keyPressed){
        if($(m+" #day").val()==""){changeInputSatus(m+" input#day");isValid=false;}
        if($(m+" #month").val()==""){changeInputSatus(m+" input#month");isValid=false;}
        if($(m+" #year").val()==""){changeInputSatus(m+" input#year");isValid=false;}
    }
    if($(m+" #day").val()*1<0 || $(m+" #day").val()*1>31 || isNaN($(m+" #day").val()*1) ){
        changeInputSatus(m+" input#day");
        isValid = false;
    }
    if($(m+" #month").val()*1<0 || $(m+" #month").val()*1>12 || isNaN($(m+" #month").val()*1)){
        changeInputSatus(m+" input#month");
        isValid = false;
    }
    if($(m+" #year").val()!="" && $(m+" #year").val()*1<1900 || isNaN($(m+" #year").val()*1) || $(m+" #year").val()!="" && $(m+" #year").val()*1>2010){
        changeInputSatus(m+" input#year");
        isValid = false;
    };
    if($(m+" #year").val().length<4 && $(m+" #year").val().length<4 && !keyPressed) changeInputSatus(m+"  input#year");
    else changeInputSatus(m+" input#year",true);
    return isValid;
}



});/*----------  TERMINA DOCUMENT READY  ----------*/



function intro_fn(){

	$('#pleca-diagonal').addClass('slide-right2');
	
	$( ".txt_mod1" ).animate({
	    top: 0,
	    bottom:0,
	    opacity:'1'
	  }, 1500, function() {

	    // Animation complete. hash-mod1
	    $(".hash-mod1").animate({
	        opacity:'1',
	    }, 2000, "easeOutExpo");
	    
	  });
}
/** FUNCTIONS SLIDERS **/

/* AVANZAR AL SIGUIENTE */
function moveSlide(){
	clearTimeout(slider_interval);
	var neg_percent = '-'+current_slide+'00%';

	if(current_slide < total_slides){//Si 1 < 1 osea que aqui automáticamente da falso

		$("#cont-items").animate({
			left: neg_percent
		},600, "easeInOutExpo",function(){
			current_slide++; //Aqui aumenta el valor a 2
			console.log(current_slide);
			if(current_slide == 1){
				$('.slider1 .next').show();
				$('.slider1 .prev').hide();
			} else{
				$('.slider1 .next').hide();
				$('.slider1 .prev').show();
			}

			
		});

	}else{ // y aqui es en el caso de que estemos siempre en el segundo slide
		$("#cont-items").animate({
			left: '0%'
		},600, "easeInOutExpo",function(){
			current_slide = 1;
			console.log(current_slide);
			if(current_slide == 1){
				$('.slider1 .next').show();
				$('.slider1 .prev').hide();
			} else{
				$('.slider1 .next').hide();
				$('.slider1 .prev').show();
			}

		});
	}
	//slider_interval = setTimeout("moveSlide()",20000);
	
}

/* AVANZAR AL SIGUIENTE 2 */
function moveSlide2(){
	clearTimeout(slider_interval2);
	var neg_percent2 = '-'+current_slide2+'00%';

	if(current_slide2 < total_slides2){

		$("#cont-items2").animate({
			left: neg_percent2
		},600, "easeInOutExpo",function(){
			current_slide2++;
			if(current_slide2 == 1){
				$('.slider2 .next2').show();
				$('.slider2 .prev2').hide();

			} else{
				$('.slider2 .next2').hide();
				$('.slider2 .prev2').show();
			}
			
		});

	}else{
		$("#cont-items2").animate({
			left: '0%'
		},600, "easeInOutExpo",function(){
			current_slide2 = 1;
			if(current_slide2 == 1){
				$('.slider2 .next2').show();
				$('.slider2 .prev2').hide();
			} else{
				$('.slider2 .next2').hide();
				$('.slider2 .prev2').show();
			}
		});
	}
	//slider_interval2 = setTimeout("moveSlide()",20000);
}

/* REGRESAR AL ANTERIOR */
function backSlide(){
	clearTimeout(slider_interval);
	var neg_percent = '-'+(current_slide-2)+'00%';
	
	if(current_slide == 1){

		$("#cont-items").animate({
			left: '-'+(total_slides-1)+'00%'
		},600, "easeInOutExpo",function(){
			current_slide = total_slides;
			console.log(current_slide);
			if(current_slide == 1){
				$('.slider1 .next').show();
				$('.slider1 .prev').hide();
			} else{
				$('.slider1 .next').hide();
				$('.slider1 .prev').show();
			}
		
		});

	}else{
		$("#cont-items").animate({
			left: neg_percent
		},600, "easeInOutExpo",function(){
			current_slide--;
			if(current_slide == 1){
				$('.slider1 .next').show();
				$('.slider1 .prev').hide();
			} else{
				$('.slider1 .next').hide();
				$('.slider1 .prev').show();
			}

		});
	}
	//slider_interval = setTimeout("backSlide()",20000);
}

/* REGRESAR AL ANTERIOR */
function backSlide2(){
	clearTimeout(slider_interval2);
	var neg_percent2 = '-'+(current_slide2-2)+'00%';

	if(current_slide2 == 1){
		$("#cont-items2").animate({
			left: '-'+(total_slides2-1)+'00%'
		},600, "easeInOutExpo",function(){
			current_slide2 = total_slides2;
			if(current_slide2 == 1){
				$('.slider2 .next2').show();
				$('.slider2 .prev2').hide();
			} else{
				$('.slider2 .next2').hide();
				$('.slider2 .prev2').show();
			}
		
		});

	}else{
		$("#cont-items2").animate({
			left: neg_percent2
		},600, "easeInOutExpo",function(){
			current_slide2--;
			if(current_slide2 == 1){
				$('.slider2 .next2').show();
				$('.slider2 .prev2').hide();
			} else{
				$('.slider2 .next2').hide();
				$('.slider2 .prev2').show();
			}
		});
	}
	//slider_interval2 = setTimeout("backSlide2()",20000);
}


