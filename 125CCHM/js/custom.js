$(function() { 

	$.scrollIt({
	  upKey: 38,             // key code to navigate to the next section
	  downKey: 40,           // key code to navigate to the previous section
	  easing: 'linear',      // the easing function for animation
	  scrollTime: 600,       // how long (in ms) the animation takes
	  activeClass: 'active', // class given to the active nav element
	  onPageChange: null,    // function(pageIndex) that is called when page is changed
	  topOffset: 0           // offste (in px) for fixed top navigation
	}); 


/*----------  Ajuste de contenedores al 100% del alto de la ventana  ----------*/

function setSize() {

  	windowHeight = $(window).height();

  	/*Footer height in px or decimals if you mean percentage */
	var percent = 0.15;
	var hpercent = windowHeight * percent;
   
    $('section, .nav-container').css('height', windowHeight);

    var windowWidth = $(window).width();

    if(windowWidth > 1024 && windowHeight < 1024){
    	$('.nav-container').addClass('desktop');
    } else{
    	$('.nav-container').removeClass('desktop');
    }
};
  
	setSize();

//Y para que en cada escala de la página se recalcule la altura  
$(window).resize(function() {
    setSize();
  });

/*----------  Botones para mostrar info de los slides ----------*/

$('a.more').on('click', function() {
	$(this).parent().find('.info').animate({top:0}, 300);
});

$('a.back').on('click', function() {
	$(this).parent().animate({top:'-100%'}, 300);
});

/*----------  Acciones del slider de textos y de imágenes  ----------*/

$('section#celebrate a.arrow').on('click', function(event) {
	var context = $(this).parent();
	slideshowRun(context);
});

$('section#events a.arrow').on('click', function(event) {
	var context = $(this).parent();
	slideshowRunOnlyText(context);
});

// setInterval('slideshowRun();',5000); //Por si queremos que avance automático, pues se descomenta esto

function slideshowRun(context) {
  var next = $(context).find(".content.active").next(".content");
  console.log(next);
  if($(context).find(".content.active").hasClass("last")){
    next = $(context).find(".content.first");
	}
	$(context).find('.container').fadeOut(500);
  	$(context).find('.content.active').animate({opacity:0, left:'-100%'},400, function(){
    	}).removeClass("active");
  	// $(this).find('.container').fadeOut(300);
  	
    next.animate({opacity:1, left:0},300, function(){$(this).find('.container').fadeIn(200);}).addClass("active");
}

function slideshowRunOnlyText(context) {
  var next = $(context).find(".content.active").next(".content");
  console.log(next);

  if($(context).find(".content.active").hasClass("last")){
    next = $(context).find(".content.first");
	}
	$(context).find('.content.active').removeClass("active");
	$(context).find('h2.visible').removeClass('visible');
	$(context).find('h2, span, hr, p ').animate({opacity : 0},100, function(){
	});
  	
  	next.addClass("active");
  	next.animate({opacity : 1}, 200, function(){
  		$(this).find('h2').animate({opacity: 1, top:0}, 500,function(){
  			$(this).addClass('visible');
	    	$(this).parent().find('span').animate({opacity: 1}, 100, function(){
	    		$(this).parent().find('hr').animate({opacity: 1}, 600, function(){
	    			$(this).parent().find('p').animate({opacity: 1}, 700, function(){
	    				
	    			});
	    		});
	    	});
	    });
  	});
    

	}

	/*================================================
	=            Scripts para el timeline            =
	================================================*/

	var sum = 0;
	$("#years li").each(function() {
		sum += $(this).width();
    // sum += $(this).width() + parseInt($(this).css('paddingLeft')) + parseInt($(this).css('paddingRight'))
	});
	$("#years").css('width', sum-50);

	$("#holder").mousemove(function(e) {

    x = -(((e.pageX - $('#years').position().left) / $("#holder").width()) * ($("#years").width() - $("#holder").width()));

    $("#years").css({
        'marginLeft': x + 'px'
    });

     var sizeWidth = $(window).width;

	if(sizeWidth > 1025){

	    if(x<= -1900){
	    	console.log('límite derecho cerca');
	    	$('.nav-container, .paneTwo').addClass('limit');
	    } else if(x >= -1900) {
	    	console.log('límite derecho lejos');
	    	$('.nav-container, .paneTwo').removeClass('limit')
	    }
	    if(x >= -40) {
	    	console.log('límite izquierdo cerca');
	    	$('.paneOne').addClass('limit');
	    } 
	    else if(x <= -40){
	    	$('.paneOne').removeClass('limit');
	    }
	}
});

/*----------  Scripts para eventos Touch  ----------*/

$('#holder').bind('touchmove',function(e){
      e.preventDefault();
      var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
      var elm = $(this).offset();
      var xP = touch.pageX - elm.left;
      var yP = touch.pageY - elm.top;
      if(xP < $(this).width() && xP > 0){
          if(yP < $(this).height() && yP > 0){
	            //CODE GOES HERE

			    x = -(((touch.pageX - $('#years').position().left) / $("#holder").width()) * ($("#years").width() - $("#holder").width()));

			    $("#years").css({
			        'marginLeft': x + 'px'
			    });

			    }   
          	}
		});

});//---------------------------Aqui termina la función jQuery ready--------------------------------//

/*----------  Script para efecto Masonry items y carga progresiva con isotope ----------*/

var $grid = $('.grid').isotope({
  // set itemSelector so .grid-sizer is not used in layout
  itemSelector: '.grid-item',
  percentPosition: true,
  masonry: {
    // use element for option
    columnWidth: 200,
    gutter: 10
  }
});

/*----------  Carga de más elementos en el feed  ----------*/
 
  $('.append-button').on( 'click', function() {
    // create new item elements
    var $items = getItemElement().add(getItemElement()).add(getItemElement());
    // append elements to container
    $grid.append($items)
      // add and lay out newly appended elements
      .isotope( 'appended', $items );
  
  });

function getItemElement() {
  var $item = $('<div class="grid-item"></div>');
  // add width and height class
  var hRand = Math.random();
  // make <div class="grid-item--height#" />
  var heightClass = hRand > 0.85 ? 'grid-item--height3' : hRand > 0.5 ? 'grid-item--height2' : '';
  $item.addClass( heightClass );
  return $item;
}

/*----------  Animación del menú responsive  ----------*/


$('.menuToggle').on('click', makeResponsive);

$('.nav-container ul li a').on('click',  makeResponsive);

function makeResponsive(){
	if($('.menuToggle').hasClass('on')){
		$('.menuToggle').find('#nav-icon').removeClass('open');
		$('.nav-container').fadeOut(400);
		$('.menuToggle').removeClass('on');
	} else{
		$('.menuToggle').find('#nav-icon').addClass('open');
		$('.nav-container').fadeIn(200);
		$('.menuToggle').addClass('on');
	}
}
