$(function(){
	return;
$('#image1').reel();
//	$('#image2').reel();
//	$('#image3').reel();
});

$(document).ready(function(){

	var ancho_pantalla=$(window).width(),
		entre_a_exp=false,
		video_fondo=$('<video id="video-background" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0"><source src="video/Foto-Principal_4.webm" type="video/webm"><source src="video/Foto-Principal_4.mp4" type="video/mp4"><source src="video/Foto-Principal_4.ogv" type="video/ogg"></video>'),
		sudoSlider='';
	function voyAreel(has,frame){
		var desde=$('.panorama').find('div.actual'),
			hasta=$(has).parent("div");
		var compara_desde=desde.attr('id'),
			compara_hasta=hasta.attr('id');
		if(compara_desde!=compara_hasta){
			$(has).reel("frame",frame);
			desde.removeClass('actual').addClass('saliendo');
			hasta.show().addClass('entrando');
			setTimeout(function(){
				desde.hide().removeClass('saliendo');
				hasta.removeClass('entrando').addClass('actual');
			},1000);
		}
	}

	/*----------  Ajuste del 100 de alto del panorama en versión responsive  ----------*/

	// function setHeight() {

	//  var alto_documento = $(window).height();
	//  var ancho_documento = $(window).width();

	//  if(ancho_documento < 800 || alto_documento < 800){
	// 	  $('.panorama, .panorama .reel').css('height', alto_documento);
	// 	}
	//   };
	 
	//   setHeight();
	  
	//  $(window).resize(function() {
	//     setHeight();
	//   });

	/*----------  Responsive  ----------*/

	function toggleResponsiveStyles(){ //Crea y destruye elementos y clases dinámicamente según tamaño de pantalla

		//Footer responsive
		var alto = $(window).height();
		var ancho = $(window).width();
		if(ancho < 1000 || alto < 629){ 
			$('.mainFooter').addClass('collapsed');
			if($('.mainFooter .footer button').length == 0){
				$('.mainFooter.collapsed .footer').append('<button></button>');
			}
		} else {
			$('.mainFooter').removeClass('collapsed');
			$('.footer').find('button').remove();
		}

		//Menú mapa responsive
		if((ancho < 769) && $('#mapaResponsive .lugares ul').length == 0){ 
			var mapa = $('#wrapper .contiene-panorama .centr .todo .mapa .lugares').html();
			console.log(mapa);
			$('#mapaResponsive .lugares').append(mapa);
		}
		if((ancho >= 769) && $('#mapaResponsive .lugares ul').length == 1){
			$('#mapaResponsive .lugares').html('');
		}

	};

	toggleResponsiveStyles();

	jQuery(window).resize(function() {
		toggleResponsiveStyles();
	});

	/*----------  Acciones de botones vista Responsive  ----------*/
	

	$('.openMap').on('click', function(event) {
		event.preventDefault();
		$('#mapaResponsive').slideDown(500).addClass('abierto');
		$('img.logoResponsive').fadeOut(500);
	});

	$('.openMenu').on('click', function(event) {
		event.preventDefault();
		$('#socialesResponsive').slideDown(300).addClass('abierto');
	});

	$('.cierraPanel').on('click', function(event) {
		event.preventDefault();
		$('#socialesResponsive.abierto ,#mapaResponsive.abierto ').slideUp(300).removeClass('abierto');
		$('img.logoResponsive').fadeIn(500);
	});

	$('.mainFooter .footer').on('click','button', function(event) {
		event.preventDefault();
		$(this).toggleClass('on');
		$(this).parent().parent().toggleClass('full');
	});

	/*----------  Funciones generales  ----------*/
	
	function creaGalerya(cual){

		setTimeout(function(){
					$("div.cargandoGalerias").fadeOut('slow');
			},1500); 

		var vA='<iframe id="ytplayer" src="https://www.youtube.com/embed/',
			vC='<iframe width="560" height="349" src="https://www.youtube.com/embed/',
			vD='?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>',
			vB='?autoplay=1&rel=0&showinfo=0&controls=0&hd=1&border=0&wmode=opaque&enablejsapi=1&version=3&modestbranding=1&playerapiid=ytplayer" frameborder="0" allowfullscreen></iframe>',
			gal='';

		// Este video no se donde irá Y4ofYX7W5tg habrá que preguntar...
		if(cual=='Heroes'){
			$("#sudo").addClass('video').html(vA + 'ahhoDMGLzk8' + vB);
		}
		else if(cual=='Drone'){
			$("#sudo").addClass('video').html(vA + 'Z7QP6X_MBqk' + vB);
		}
		else if(cual=='Cardboard'){
			$("#sudo").addClass('video').html(vA + 'Kk9UfrjcNCo' + vB);
		}
		else if(cual=='Back'){
			$("#sudo").addClass('video').html(vA + '1N5GA38lkYY' + vB);
		}
		else if(cual=='Cinemagraphs'){
			sudoSlider=$("#sudo").sudoSlider({
					ajax:['img/galerias/Long-Live-sol.gif',
						'img/galerias/Escenario.gif',
						'img/galerias/The-wookies.gif',
						'img/galerias/elefante.gif'],
					numeric:true,
					responsive:true,
					continuous:true,
					touch:true,
					updateBefore:true,
					prevNext:true
				});
		}
		else{
			if(cual=='Descanso'){
				sudoSlider=$("#sudo").sudoSlider({
					ajax:['img/galerias/Arcadia-Area-de-descanso.jpg',
						'img/galerias/Arcadia-Area-de-descanso2.jpg',
						'img/galerias/Arcadia-Area-de-descanso3.jpg',
						'img/galerias/Arcadia-Area-de-descanso4.jpg',
						'img/galerias/Arcadia-Area-de-descanso5.jpg',
						'img/galerias/Arcadia-Area-de-descanso6.jpg',
						'img/galerias/Arcadia-Area-de-descanso7.jpg'],
					numeric:true,
					responsive:true,
					continuous:true,
					touch:true,
					updateBefore:true,
					prevNext:true
				});
			}
			else if(cual=='Centinelas'){
				sudoSlider=$("#sudo").sudoSlider({
					ajax:['img/galerias/centinelas-1.jpg',
						'img/galerias/centinelas-2.jpg',
						'img/galerias/centinelas-3.jpg',
						'img/galerias/centinelas-4.jpg',
						'img/galerias/centinelas-5.jpg',
						'img/galerias/centinelas-6.jpg',
						'img/galerias/centinelas-7.jpg',
						'img/galerias/centinelas-8.jpg',
						'img/galerias/centinelas-9.jpg',
						'img/galerias/centinelas-10.jpg',
						'img/galerias/centinelas-11.jpg',
						'img/galerias/centinelas-12.jpg',
						'img/galerias/centinelas-13.jpg'],
					numeric:true,
					responsive:true,
					continuous:true,
					touch:true,
					updateBefore:true,
					prevNext:true
				});
			}
			else if(cual=='Videos'){
				sudoSlider=$("#sudo").addClass('videoembed').html('<div><div class="video-responsive">'+ vC + 'cYcL_eQOMCA'+ vD + '</div></div><div><div class="video-responsive">'+ vC + '77JREOlqIdY'+ vD + '</div></div>').sudoSlider({
					numeric:true,
					responsive:true,
					continuous:true,
					touch:true,
					updateBefore:true,
					prevNext:true
				});
			}
		}
	}
	if(ancho_pantalla>1024)
		$(video_fondo).insertBefore('.home');

//Menus de mapa y banderitas
	$('body').on('click','.reel-annotation,.lugares ul li a', function(event){
		event.preventDefault();
		var para=$(this).attr('para'),
			frame=$(this).attr('frame'),
			tipo=$(this).attr('destino-galeria'),
			galeria=$(this).attr('class');
		$('.lugares ul li').removeClass('active');
		$('.lugares ul li a[para="' + para + '"]').closest("li").addClass('active');
		para='#' + para;
		voyAreel(para,frame);
		if(tipo!==undefined){
			$('#galeria').show();
			creaGalerya(tipo);
		}
	});

//Cerrar galerias
	$('#galeria .cerrar').click(function(event){
		$('#galeria').hide();
		$("div.cargandoGalerias").show();
		$("iframe").each(function(){ this.remove(); });
		$("#sudo img").each(function(){ this.remove(); });
		$('#sudo').removeClass('video').removeAttr('style');
		if(sudoSlider)
			sudoSlider.destroy();
		$('#sudo').empty();
	});

//Entra a experiencia
	$('.vive_exp').click(function(event){
		$("body").addClass('exp');
		$("#wrapper,div.contiene-home").addClass('ya');
		entre_a_exp=true;
		setTimeout(function(){
			$("#wrapper").addClass('fin');
			$("div.contiene-home").remove();
		},1300);
	});
	$(window).on('resize',function(){
		ancho_pantalla=$(window).width();
		if(ancho_pantalla<1024){
			$("video").each(function(){ this.remove(); });
		}
		else{
			if(entre_a_exp)
				$("video").each(function(){ this.remove(); });
			else{
				$(video_fondo).insertBefore('.home');
				$("video").each(function(){ this.play(); });
			}
		}
	});
});
$(window).load(function(){
	setTimeout(function(){
		$('body').toggleClass('loaded');
		$("div.cargando").remove();
		$("#image2-reel,#image3-reel,#image4-reel,#image5-reel,#image6-reel,#image7-reel,#image8-reel,#image9-reel,#image10-reel").hide();
	},800);

	if(window.innerHeight > window.innerWidth){
    	$('#turnPhone').fadeIn('fast');
	}
});


/*----------  Eventos en el cambio de orientación del dispositivo  ----------*/

$( window ).on( "orientationchange", function( event ) {
  if(event.orientation == "portrait"){
  	$('#turnPhone').fadeIn('fast');
  }
  if(event.orientation == "landscape"){
  	$('#turnPhone').fadeOut('fast');
  }
});
