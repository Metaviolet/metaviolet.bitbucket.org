/*======================================
=            Deck de cartas            =
======================================*/
$(window).load(function() {

var cards = ['bicycle', 'leaf', 'cube', 'anchor',  'paper-plane-o', 'bolt', 'diamond','child','flask','umbrella','tree','rocket','truck','glass','bug','bell','cog','futbol-o'];
var opend = [];
var match = [];
var $deck = $('.deck');

//cards.sort(function() { return 0.5 - Math.random() });
for(var i=0; i < cards.length;i++) {
  $deck.append($('<li class="card unabled"><i class="fa fa-' + cards[i] + '"></i></li><li class="card unabled"><i class="fa fa-' + cards[i] + '"></i></li>')); 
}

//SHUFFLE PLUGIN
$.fn.shuffle = function() {
        var allElems = this.get(),
            getRandom = function(max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function(){
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
           });

        this.each(function(i){
            $(this).replaceWith($(shuffled[i]));
        });

        return $(shuffled);
    };

//SHUFFLE CARDS
$('.card').shuffle();

//Guardar estadísticas
  var errores = 0;
  var aciertos = 0;
  var puntos = 0;

//ACTIONS ON CLICK

$deck.on('click', '.card:not(".match, .open, .unabled")', function() {
  var card = $(this).context.innerHTML;
	var $this = $(this);
  opend.push(card);
	$this.addClass('open');
	setTimeout(function() {
			$this.addClass('show');
	}, 50);
	
  if(opend.length > 1) {
    if(card === opend[0]) {
			$deck.find('.open').addClass('match');
			setTimeout(function() {
      	$deck.find('.match').removeClass('open show');
			}, 300);
			match.push(opend[0], opend[1]);
			aciertos++;
			puntos++;
    } else {
			$deck.find('.open').addClass('notmatch');
			setTimeout(function() {
      	$deck.find('.open').removeClass('open show notmatch');
			}, 300);
			errores++;
		}

		opend = [];
  }

//Mostrar estadísticas
		// $('#aciertos').html(aciertos);
		// $('#errores').html(errores);
		puntos = aciertos*50;
		$('#puntos').html(puntos);
		if(aciertos === cards.length){
			success(errores,puntos);
		}
		console.log('Aciertos: '+aciertos+', Errores: '+errores+', Puntos: '+puntos);
		return errores;

//función al completar el memorama	
function success(e,p){
	$('.alert').text('¡Felicidades! completaste el juego');
	$('.stats').hide();
	$('.stats:nth-of-type(4)').show();
	$('.final_points').text(p);
	$('.overlay.error').fadeIn(500);
	clock.stop();
}
});

/*================================
=    Funcionamiento Contador      =
================================*/

	var clock;
	var dif = 120;
	console.log(dif);
	clock = $('.countdown').FlipClock({
		clockFace:'MinuteCounter',
		autoStart:false,
		callbacks:{
			interval: function(){
				var time  = clock.getTime();
				console.log("Tu tiempo"+ time+ ", y tus Errores"+errores);
					if(time==0){
						clock.stop();
						$('.alert').text('Se acabó tu tiempo');
						$('.final_errors').text(errores);
						$('.final_time').text('0');
						$('.final_points').text(puntos);
						$('.overlay.error').fadeIn(500);
					}

					if(errores==35){
						//console.log("Tu tiempo"+ time+ ", Errores"+errores);
						clock.stop();
						$('.alert').text('Alcanzaste el máximo de errores');
						$('.final_errors').text(errores);
						$('.final_time').text(time);
						$('.final_points').text(puntos);
						$('.overlay.error').fadeIn(500);
					}
					
				}
			}
		});
		clock.setTime(dif);
		clock.setCountdown(true);

		//función play
		$('body').on('click', 'button.play',function() {
			$('.card.unabled').removeClass('unabled');
			$(this).removeClass('play').addClass('pause');
			clock.start();

		//Función pausa
		$('body').on('click','button.pause', function() {
			clock.stop();
			$('.card').shuffle().addClass('unabled');
			$(this).removeClass('pause').addClass('play');
		});
	});

		//Función cerrar modal y resetear valores
		$('.cerrar').on('click', function() {
			clock.setTime(dif);
			clock.setCountdown(true);
			$('.card').shuffle().addClass('unabled').removeClass('match open show notmatch');
			$('.pause').removeClass('pause').addClass('play');
			$('.alert').text('');
			$('.stats').show();
			$('.final_errors').text('');
			$('.final_time').text('');
			$('.final_points').text('');
			$('#puntos').html('0');
			$('.overlay').fadeOut(500);
			 errores = 0;
			 aciertos = 0;
			 puntos = 0;
			 return errores;
			 console.log('Reseteando valores de Aciertos: '+aciertos+', Errores: '+errores+', Puntos: '+puntos);

		});
}); //Termina función Load

//Función pista
// $('.hint').on('click', function(event) {
// 	event.preventDefault();
// 	$('.card:not(".match, .unabled")').addClass('show open');
// 	setTimeout(function(){
// 		$('.card').removeClass('show open');
// 	},1000);
// 	/* Act on the event */
// });


